# FillyRadioApi 
**Connects LiquidSoap to Firebase Realtime Database for Metadata**

## Brief overview
**Environment Variables**
* GOOGLE_APPLICATION_CREDENTIALS
		*Used for providing the service-account.json file location*

**Features**
* Firebase Authentication for DJ logins and secure Metadata Updates
* Supports multiple stations with one instance
* Can split `title` or `song` into `artist` and `title`
* Can fetch covers from [FillyRadioCoverGrab](https://gitlab.com/fillydelphia-radio/fillyradiocovergrab/)

**Firebase Services Used**
* Firebase Authentication
* Firebase Realtime Database

Use the Frebase Realtime Database client library on your website or apps to get realtime metadata events pushed to your clients.