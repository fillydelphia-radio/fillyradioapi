const { auth } = require("./firebase/client");
const { createError } = require("http-json-errors");

const doLogin = async (req, res, next) => {
  let creds =
    req.get("authorization") || res.status(401).json(createError(401));
  try {
    var credentials = new Buffer.from(creds.split(" ").pop(), "base64")
      .toString("ascii")
      .split(":");
    (await auth(credentials[0], credentials[1]))
      ? next()
      : res.status(401).json(createError(401));
  } catch {
    res.status(401).json(createError(401));
    console.log("Login Failed - No Authorization Provided");
  }
};

const doDjLogin = async (req, res, next) => {
  let { user, password } = req.body;
  if (user == "source") {
    user = password.split("|")[0];
    password = password.split("|")[1];
  }
  try {
    (await auth(user, password))
      ? next()
      : res.status(401).json(createError(401));
  } catch {
    res.status(401).json(createError(401));
    console.log("Login Failed - No Authorization Provided");
  }
};

exports.doLogin = doLogin;
exports.doDjLogin = doDjLogin;
//exports.doAdminLogin = doAdminLogin;
