const { auth } = require("../firebase/client");
const { createError } = require("http-json-errors");

const doAdminLogin = async (req, res, next) => {
  const stations = res.locals.stations;
  let creds =
    req.get("authorization") || res.status(401).json(createError(401));
  console.log(creds);
  try {
    var credentials = getCredsFromAuthorization(creds);
    let isAuth = await auth(credentials[0], credentials[1]);
    if (isAuth && stations[req.params.shortcode].email == credentials[0]) {
      req.stationDbCode = stations[req.params.shortcode].dbcode;
      next();
    } else {
      res
        .status(401)
        .json(
          createError(401, "You are not authorized to access this resource.")
        );
    }
  } catch (e) {
    console.log("Login Failed: " + e);
    res.status(401).json(createError(401));
  }
};

function getCredsFromAuthorization(creds) {
  return new Buffer.from(creds.split(" ").pop(), "base64")
    .toString("ascii")
    .split(":");
}

exports.doAdminLogin = doAdminLogin;
