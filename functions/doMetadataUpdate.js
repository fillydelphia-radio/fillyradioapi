const axios = require("axios");
const { metaRef, db } = require("../firebase/admin");

const doMetadataUpdate = async (
  metaBody,
  stationDbCode,
  station,
  metadataCache
) => {
  // first, let's sanitize the metadata
  // We get a lot of invalid propnames, so to avoid escape-hell...
  let metadata = {};
  Object.keys(metaBody).forEach((i) => {
    metadata = {
      ...metadata,
      [i.toLowerCase().replace(/[^a-zA-Z0-9. ]/g, "")]: metaBody[i],
    };
  });
  // now it's all in lowercase, with no special characters.

  // now we need to create an object with all the possible values we could want

  let substMetadata = (({
    title,
    artist,
    icename,
    icedescription,
    iceurl,
    isLive,
    song,
    onair,
    album,
    date,
    comment,
    store,
    wwwpublisher,
  }) => ({
    title,
    artist,
    icename,
    icedescription,
    iceurl,
    isLive,
    song,
    onair,
    album,
    date,
    comment,
    store,
    wwwpublisher,
  }))(metadata);

  // whew, that's a list. We could add more, but we need to keep data usage low
  // now, we need to fiddle with some more fields

  // we need to determine if this metadata is from a Live DJ or Auto DJ

  metadata.icename
    ? (substMetadata.isLive = true)
    : (substMetadata.isLive = false);

  // we want to know when the song was played. We don't get this when a DJ is live,
  // so we'll shove one in. Better late than never

  metadata.onair ? null : (substMetadata.onair = Date.now());

  // sometimes software only sends a "song" or "Title". We need to parse this to get the artist.
  // It's not pretty :/

  if (substMetadata.isLive) {
    if (substMetadata.artist == undefined && substMetadata.title) {
      substMetadata.artist = substMetadata.title.split(" - ")[0];
      substMetadata.title = substMetadata.title.split(" - ")[1];
    } else if (substMetadata.artist == undefined && substMetadata.song) {
      substMetadata.artist = substMetadata.song.split(" - ")[0];
      substMetadata.title = substMetadata.song.split(" - ")[1];
    }
  }

  // We should generally try to get listener counts, although we don't really
  // put much stock into them, they're good for seeing if a song is bombing or if
  // we're getting slammed.

  await axios
    .get(station.icydata)
    .then((result) => {
      let a = 0;
      result.data.icestats.source.forEach((e) => {
        a += e.listeners;
      });
      substMetadata.listeners = a;
    })
    .catch(() => {
      console.log("could not reach icecast");
      substMetadata.listeners = 0;
      return false;
    });

  // we also need to interpret the comments. BandCamp releases have a buy link, soooo:
  // BandCamp comments are something like "Visit https://westj.bandcamp.com/"
  if (substMetadata.isLive == false) {
    if (metadata.comment) {
      if (metadata.comment.includes("Visit ")) {
        substMetadata.wwwpublisher = metadata.comment.split(" ")[1];
        substMetadata.store = "BandCamp";
      }
    }
  }

  // and Pony.FM have source links sometimes:
  if (metadata.audiosourcewebpage) {
    if (metadata.audiosourcewebpage.includes("pony.fm")) {
      substMetadata.wwwpublisher = metadata.audiosourcewebpage;
      substMetadata.store = "Pony.FM";
      // we'll also pinch a value from this string...
      try {
        substMetadata.ponyfmid = metadata.audiosourcewebpage
          .split("tracks/")[1]
          .split("-")[0];
      } catch {
        substMetadata.ponyfmid = null;
      }
      console.log(substMetadata.ponyfmid);
    }
  }

  // for everything else, we gotta manually add the data :/

  // onto album covers. Now this one will be an external program
  // We'll pull the covers from a URL specified in the stations
  // "externalCoverUrl"

  const dummyCovers = {
    [96]: `https://dummyimage.com/96.jpg`,
    [128]: `https://dummyimage.com/128.jpg`,
    [192]: `https://dummyimage.com/192.jpg`,
    [256]: `https://dummyimage.com/256.jpg`,
    [384]: `https://dummyimage.com/384.jpg`,
    [512]: `https://dummyimage.com/512.jpg`,
    original: `https://dummyimage.com/1000.jpg`,
  };

  console.log({
    filePath: metaBody.filename,
    basePath: station.basepath,
    defaultImage: station.cover,
    isLive: substMetadata.isLive,
    metadata: {
      title: substMetadata.title,
      artist: substMetadata.artist,
    },
  });

  let coverhash = await axios({
    url: station.coverApi,
    method: "POST",
    data: {
      filePath: metaBody.filename,
      basePath: station.basepath,
      defaultImage: station.cover,
      isLive: substMetadata.isLive,
      metadata: {
        title: substMetadata.title,
        artist: substMetadata.artist || false,
        liveCover: metadata.iceurl,
      },
    },
  }).catch((e) => {
    console.log(e);
    return { data: false };
  });

  if (coverhash.data.hash) {
    substMetadata.covers = {
      [96]: `${station.externalCoverUrl}${coverhash.data.hash}-96.jpg`,
      [128]: `${station.externalCoverUrl}${coverhash.data.hash}-128.jpg`,
      [192]: `${station.externalCoverUrl}${coverhash.data.hash}-192.jpg`,
      [256]: `${station.externalCoverUrl}${coverhash.data.hash}-256.jpg`,
      [384]: `${station.externalCoverUrl}${coverhash.data.hash}-384.jpg`,
      [512]: `${station.externalCoverUrl}${coverhash.data.hash}-512.jpg`,
      original: `${station.externalCoverUrl}${coverhash.data.hash}.jpg`,
    };
  } else {
    substMetadata.covers = dummyCovers;
  }

  // Last chance to have "critical" props set if missing

  substMetadata.title = substMetadata.title
    ? substMetadata.title
    : "Unknown Title";
  substMetadata.artist = substMetadata.artist
    ? substMetadata.artist
    : "Unknown Artist";

  // Neat trick I just found out, you can get rid of all the undefined
  // stuff with JSON.stringify!

  substMetadata = JSON.parse(JSON.stringify(substMetadata));

  // finally, we need to handle the history.

  const currMeta = await metaRef.once("value");
  history = currMeta.val()[stationDbCode]["now-playing"].history;

  // history[0] is always the same as the current song. Not for any reason
  // other than making it easier to deal with

  // now... we add to the history object, and cut off the 6th entry

  // We have to stringify the data to JSON, then parse it, to avoid a circular object... wtf

  let interim = JSON.stringify(substMetadata);

  history.unshift(JSON.parse(interim));

  // SERIOUSLY, WHAT THE FUCK?

  history.length > 5 ? history.pop() : null;

  substMetadata.history = history;

  // We can now push this data to Firebase!

  db.ref(`rest/${stationDbCode}/now-playing`).set(substMetadata);

  return substMetadata;
};

module.exports.doMetadataUpdate = doMetadataUpdate;
