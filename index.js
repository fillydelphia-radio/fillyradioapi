const { doAdminLogin } = require("./functions/doAdminLogin");

const express = require("express");
const bodyParser = require("body-parser");
const { doDjLogin } = require("./middleware");
const { stationRef } = require("./firebase/admin");
const { doMetadataUpdate } = require("./functions/doMetadataUpdate");

const router = express();
const metadataCache = {};

let stations = {};

stationRef.on(
  "value",
  function (snapshot) {
    stations = snapshot.val();
  },
  function (e) {
    console.log(e);
    return {};
  }
);

function attachStationObject(req, res, next) {
  res.locals.stations = stations;
  next();
}

router.use(bodyParser.json());

router.get("/", attachStationObject, async (req, res, next) => {
  res.json({
    status: "ok",
    serverTime: Date.now(),
    metadata: metadataCache,
    stations,
  });
});

// special route just for LiquidSoap
router.post("/djlogin", doDjLogin, (req, res, next) => {
  // LS literally parses the returned plaintext as a boolean
  res.send("true");
});

router.post(
  "/updatemeta/:shortcode",
  attachStationObject,
  doAdminLogin,
  async (req, res, next) => {
    res.json({
      isHttpError: false,
      statusCode: 200,
      title: "Updated Metadata",
      message: `Metadata Update Request Received: ${req.body.artist} - ${req.body.title}`,
      body: {
        message: `Metadata Update Request Received: ${req.body.artist} - ${req.body.title}`,
      },
    });

    await checkDoubleReport(req);
  }
);

const port = process.env.PORT;

router.listen(port || 3000, () => {
  console.log("App started on " + (port || 3000));
});

async function checkDoubleReport(req) {
  if (metadataCache[req.params.shortcode] == undefined) {
    metadataCache[req.params.shortcode] = await doMetadataUpdate(
      req.body,
      req.stationDbCode,
      stations[req.params.shortcode],
      metadataCache
    );
    console.log(metadataCache[req.params.shortcode]);
  } else if (metadataCache[req.params.shortcode].title !== req.body.title) {
    metadataCache[req.params.shortcode] = await doMetadataUpdate(
      req.body,
      req.stationDbCode,
      stations[req.params.shortcode]
    );
    console.log(metadataCache[req.params.shortcode]);
  }
}
