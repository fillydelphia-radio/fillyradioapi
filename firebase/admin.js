const admin = require("firebase-admin");

admin.initializeApp({
  credential: admin.credential.applicationDefault(),
  databaseURL: "https://fillydelphia-radio.firebaseio.com",
});

var db = admin.database();
var stationRef = db.ref("stations");
var metaRef = db.ref("rest");

module.exports.stationRef = stationRef;
module.exports.metaRef = metaRef;
module.exports.db = db;
