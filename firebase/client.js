const firebase = require("firebase");

const config = {
  apiKey: "AIzaSyCkc8MAShqq2IXB7mEJCvwwL4MRVhmrdd0",
  authDomain: "fillydelphia-radio.firebaseapp.com",
  databaseURL: "https://fillydelphia-radio.firebaseio.com",
  projectId: "fillydelphia-radio",
  storageBucket: "fillydelphia-radio.appspot.com",
  messagingSenderId: "404657910771",
};

firebase.initializeApp(config);

const auth = async (username, password) => {
  return firebase
    .auth()
    .signInWithEmailAndPassword(username, password)
    .then(() => {
      console.log(`${username} signed in successfully`);
      return true;
    })
    .catch((e) => {
      console.log(`${username} failed to sign in. ${e.message}`);
      return false;
    });
};

module.exports.auth = auth;
